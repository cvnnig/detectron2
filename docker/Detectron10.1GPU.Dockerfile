FROM nvidia/cuda:10.1-cudnn7-devel

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y \
	vim python3-opencv ca-certificates python3-dev git wget sudo  \
	cmake protobuf-compiler libprotobuf-dev && \
  rm -rf /var/lib/apt/lists/*
RUN ln -sv /usr/bin/python3 /usr/bin/python

# create a non-root user
ARG USER_ID=1000
RUN useradd -m --no-log-init --system  --uid ${USER_ID} appuser -g sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
USER appuser
WORKDIR /home/appuser

ENV PATH="/home/appuser/.local/bin:${PATH}"
RUN wget https://bootstrap.pypa.io/get-pip.py && \
	python3 get-pip.py --user && \
	rm get-pip.py

# install dependencies
# See https://pytorch.org/ for other options if you use a different version of CUDA
RUN pip install --user torch torchvision tensorboard cython
RUN pip install --user 'git+https://github.com/cocodataset/cocoapi.git#subdirectory=PythonAPI'

RUN pip install --user 'git+https://github.com/facebookresearch/fvcore'

# install detectron2
RUN git clone https://github.com/facebookresearch/detectron2 detectron2_repo
ENV FORCE_CUDA="1"
# This will build detectron2 for all common cuda architectures and take a lot more time,
# because inside `docker build`, there is no way to tell which architecture will be used.
ENV TORCH_CUDA_ARCH_LIST="Kepler;Kepler+Tesla;Maxwell;Maxwell+Tegra;Pascal;Volta;Turing"
RUN pip install --user -e detectron2_repo

# install opencv.
RUN pip install --user opencv-python

# Have vim is easier for debugging. To be removed in production build.

# Set a fixed model cache directory.
ENV FVCORE_CACHE="/tmp"

# Copy over main CVNNIG repo. Overwrite the .env that points to /tmp.
ADD ./cvnnig/ /cvnnig/
ADD ./cvnnig/DockerFiles/.env /cvnnig/.env

# Update the system python path to
ENV PYTHONPATH "${PYTHONPATH}:/cvnnig/"

# Install minor dependencies
WORKDIR /cvnnig/depends/
RUN pip install --user -r requirements.txt

###############################################
# Test Run: Run detectron2 under user "appuser":
###############################################s
# First Login:
# docker run --gpus all -it --shm-size=8gb --env="DISPLAY" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" --name=detectron2 detectron2:v1
# docker run --gpus all -it --shm-size=8gb --env="DISPLAY" --volume="/tmp/.X11-unix:/tmp/.X11-unix:rw" -v="/media/dyt811/A4FCE3EEFCE3B8A6/Git/bengali.ai/data/raw:/tmp/Test:rw" --name=detectron2 cvnnig/detectron2:v0.0.1
# Test 1:
# wget http://images.cocodataset.org/val2017/000000439715.jpg -O input.jpg
# python demo/demo.py --config-file configs/COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x.yaml --input input.jpg --output outputs/output.jpg --opts MODEL.WEIGHTS detectron2://COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_3x/137849600/model_final_f10217.pkl

# Test 2:
ADD ./detectron2/docker/test.py /test/
# python3 /test/test.py

###############################################
## To build:
###############################################
# cd docker/
# docker build --build-arg USER_ID=$UID -t cvnnig/detectron2:v0.0.2 -f Detectron10.1GPU.Dockerfile /home/dyt811/Git/